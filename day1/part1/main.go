package main

import "fmt"

func main() {
	freq := 0

	for {
		var v int
		if _, err := fmt.Scanf("%d", &v); err != nil {
			break
		}

		freq += v
	}

	fmt.Print(freq)
}
