import sys

instructions = []
for line in sys.stdin:
    instructions.append(int(line))

seen_totals = set()
running_total = 0
seen_totals.add(0)
while True:
    for v in instructions:
        running_total += v
        if running_total in seen_totals:
            print(running_total)
            exit()
        seen_totals.add(running_total)
