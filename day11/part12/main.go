package main

import "fmt"

func power(x, y, serial int) int {
	rack := x + 10
	total := (rack*y + serial) * rack
	return (total / 100 % 10) - 5
}

type ArrayMap [][]int

func NewArrayMap(h, w int) ArrayMap {
	result := make([][]int, h)
	for y := 0; y < h; y++ {
		result[y] = make([]int, w)
	}
	return result
}

func (am *ArrayMap) Get(x, y int) int {
	if x < 0 || y < 0 {
		return 0
	}
	return (*am)[y][x]
}

func (am *ArrayMap) Set(x, y, value int) {
	(*am)[y][x] = value
}

func (am *ArrayMap) Update(x, y, value int) {
	(*am)[y][x] += value
}

func solve(serial, bounds, size int) (int, int, int) {
	area := NewArrayMap(bounds, bounds)

	for y := 0; y < bounds; y++ {
		for x := 0; x < bounds; x++ {
			area.Set(x, y, power(x, y, serial))
			area.Update(x, y, area.Get(x-1, y))
			area.Update(x, y, area.Get(x, y-1))
			area.Update(x, y, -area.Get(x-1, y-1))
		}
	}

	maxSum := bounds * bounds * -10
	maxX := -1
	maxY := -1

	for y := 0; y < bounds-size; y++ {
		for x := 0; x < bounds-size; x++ {
			sum := area.Get(x+size-1, y+size-1) - area.Get(x+size-1, y-1) - area.Get(x-1, y+size-1) + area.Get(x-1, y-1)
			if sum > maxSum {
				maxSum = sum
				maxX = x
				maxY = y
			}
		}
	}

	return maxSum, maxX, maxY
}

// Return value (sum, x, y, size)
func solveAllSizes(serial, bounds int) (int, int, int, int) {
	maxSum := bounds * bounds * -10
	maxX := -1
	maxY := -1
	maxSize := -1

	// This can be significantly improved using Kadane's algorithm
	for size := 3; size < bounds; size++ {
		sum, x, y := solve(serial, bounds, size)
		if sum > maxSum {
			maxSum = sum
			maxX = x
			maxY = y
			maxSize = size
		}
	}
	return maxSum, maxX, maxY, maxSize
}

func read() int {
	var serial int
	fmt.Scanf("%d", &serial)
	return serial
}

func main() {
	serial := read()
	_, x, y := solve(serial, 300, 3)
	fmt.Printf("%d,%d\n", x, y)

	_, x, y, size := solveAllSizes(serial, 300)
	fmt.Printf("%d,%d,%d\n", x, y, size)
}
