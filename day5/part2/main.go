package main

import (
	"fmt"
	"strings"
	"unicode"
)

func read() string {
	var value string
	fmt.Scanln(&value)
	return value
}

type Stack []rune

func (s *Stack) Push(el rune) {
	*s = append(*s, el)
}

func (s *Stack) Size() int {
	return len(*s)
}

func (s *Stack) Empty() bool {
	return s.Size() == 0
}

func (s *Stack) Top() rune {
	value := []rune(*s)[len(*s)-1]
	return value
}

func (s *Stack) Pop() rune {
	value := s.Top()
	*s = []rune(*s)[:len(*s)-1]
	return value
}

func equalCaseInsensitive(a, b rune) bool {
	return unicode.ToLower(a) == unicode.ToLower(b)
}

func mergeable(a, b rune) bool {
	comp := func(a, b rune) bool {
		return unicode.IsLower(a) &&
			unicode.IsUpper(b)
	}

	return (comp(a, b) || comp(b, a)) &&
		equalCaseInsensitive(a, b)
}

func reactedLength(str string) int {
	stack := make(Stack, 0)

	for _, r := range str {
		if stack.Empty() {
			stack.Push(r)
			continue
		}

		currentTop := stack.Top()
		if mergeable(r, currentTop) {
			stack.Pop()
		} else {
			stack.Push(r)
		}
	}

	return stack.Size()
}

func removeCharacter(str string, char rune) string {
	var builder strings.Builder

	for _, r := range str {
		if !equalCaseInsensitive(r, char) {
			builder.WriteRune(r)
		}
	}

	return builder.String()
}

func allCharacters(str string) []rune {
	set := make(map[rune]bool)

	for _, r := range str {
		set[unicode.ToLower(r)] = true
	}

	result := make([]rune, 0)
	for k, _ := range set {
		result = append(result, k)
	}
	return result
}

func main() {
	str := read()

	chars := allCharacters(str)

	shortest := len(str)
	for _, c := range chars {
		length := reactedLength(removeCharacter(str, c))
		if length < shortest {
			shortest = length
		}
	}

	fmt.Println(shortest)
}
