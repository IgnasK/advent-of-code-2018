package main

import (
	"fmt"
	"unicode"
)

func read() string {
	var value string
	fmt.Scanln(&value)
	return value
}

type Stack []rune

func (s *Stack) Push(el rune) {
	*s = append(*s, el)
}

func (s *Stack) Size() int {
	return len(*s)
}

func (s *Stack) Empty() bool {
	return s.Size() == 0
}

func (s *Stack) Top() rune {
	value := []rune(*s)[len(*s)-1]
	return value
}

func (s *Stack) Pop() rune {
	value := s.Top()
	*s = []rune(*s)[:len(*s)-1]
	return value
}

func mergeable(a, b rune) bool {
	comp := func(a, b rune) bool {
		return unicode.IsLower(a) &&
			unicode.IsUpper(b)
	}

	return (comp(a, b) || comp(b, a)) &&
		unicode.ToLower(a) == unicode.ToLower(b)
}

func reactedLength(str string) int {
	stack := make(Stack, 0)

	for _, r := range str {
		if stack.Empty() {
			stack.Push(r)
			continue
		}

		currentTop := stack.Top()
		if mergeable(r, currentTop) {
			stack.Pop()
		} else {
			stack.Push(r)
		}
	}

	return stack.Size()
}

func main() {
	str := read()

	fmt.Println(reactedLength(str))
}
