package main

import (
	"fmt"
)

type Point struct {
	x, y int
}

type Bounds struct {
	left, right, top, bottom int
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func NewBounds(p Point) Bounds {
	return Bounds{left: p.x, right: p.x, top: p.y, bottom: p.y}
}

func (bounds *Bounds) Extend(point Point) {
	bounds.left = min(bounds.left, point.x)
	bounds.right = max(bounds.right, point.x)
	bounds.top = min(bounds.top, point.y)
	bounds.bottom = max(bounds.bottom, point.y)
}

func (bounds *Bounds) Width() int {
	return bounds.right - bounds.left + 1
}

func (bounds *Bounds) Height() int {
	return bounds.bottom - bounds.top + 1
}

func (p Point) OnBounds(bounds Bounds) bool {
	return (p.x == bounds.left || p.x == bounds.right) &&
		(p.y == bounds.top || p.y == bounds.bottom)
}

func (p Point) InBounds(bounds Bounds) bool {
	return p.x >= bounds.left && p.x <= bounds.right &&
		p.y >= bounds.top && p.y <= bounds.bottom
}

func (p Point) Normalised(bounds Bounds) Point {
	return Point{
		x: p.x - bounds.left,
		y: p.y - bounds.top,
	}
}

func read() []Point {
	result := make([]Point, 0)
	for {
		var point Point

		_, err := fmt.Scanf("%d, %d", &point.x, &point.y)
		if err != nil {
			break
		}

		result = append(result, point)
	}

	return result
}

func pointBounds(points []Point) Bounds {
	bounds := NewBounds(points[0])

	for _, p := range points[1:] {
		bounds.Extend(p)
	}

	return bounds
}

type Queue []interface{}

func (q *Queue) Push(d interface{}) {
	*q = append(*q, d)
}

func (q *Queue) Size() int {
	return len(*q)
}

func (q *Queue) Empty() bool {
	return q.Size() == 0
}

func (q *Queue) Back() interface{} {
	return []interface{}(*q)[0]
}

func (q *Queue) Pop() interface{} {
	back := q.Back()
	*q = []interface{}(*q)[1:]
	return back
}

type State int

const (
	NotInitialised State = 0
	Visited        State = 1
	Contended      State = 2
)

type Distance struct {
	distance int
	origin   int
}

type DistanceState struct {
	state State
	Distance
}

type DistancePoint struct {
	Point
	Distance
}

type Map struct {
	distances [][]DistanceState
	bounds    Bounds
}

func NewMap(bounds Bounds) *Map {
	distanceMap := make([][]DistanceState, bounds.Width()) // Distance map is transposed (i.e. w*h)
	for i := 0; i < bounds.Width(); i++ {
		distanceMap[i] = make([]DistanceState, bounds.Height())
	}

	return &Map{
		distances: distanceMap,
		bounds:    bounds,
	}
}

func (m *Map) Set(point Point, state DistanceState) {
	pos := point.Normalised(m.bounds)
	m.distances[pos.x][pos.y] = state
}

func (m *Map) Update(point DistancePoint) bool {
	pos := point.Normalised(m.bounds)

	if point.InBounds(m.bounds) {

		mapState := m.distances[pos.x][pos.y]
		if mapState.state == NotInitialised { // First visit

			mapState.state = Visited
			mapState.Distance = point.Distance
			m.distances[pos.x][pos.y] = mapState

		} else if mapState.state == Visited &&
			mapState.origin != point.origin &&
			mapState.distance == point.distance { // Tied visit

			mapState.state = Contended
			m.distances[pos.x][pos.y] = mapState

		} else { // Longer distance
			return false
		}

		return true
	}

	return false // Outside bounds
}

func (m *Map) Print() {

	for y := 0; y < m.bounds.Height(); y++ {
		for x := 0; x < m.bounds.Width(); x++ {
			state := m.distances[x][y]

			if state.state == NotInitialised {
				fmt.Print("\t.")
			} else if state.state == Visited {
				fmt.Printf("\t%d", state.distance)
			} else {
				fmt.Print("\t-")
			}
		}
		fmt.Println()
	}
}

func bfs(points []Point, mapBounds Bounds) *Map {

	distanceMap := NewMap(mapBounds)

	q := make(Queue, 0)
	for id, p := range points {
		dp := DistancePoint{
			Point: p,
			Distance: Distance{
				distance: 0,
				origin:   id,
			},
		}

		distanceMap.Set(p, DistanceState{
			state: Visited,
			Distance: Distance{
				distance: 0,
				origin:   id,
			},
		})

		q.Push(dp)
	}

	for !q.Empty() {
		p := q.Pop().(DistancePoint)

		for dx := -1; dx <= 1; dx++ {
			for dy := -1; dy <= 1; dy++ {
				if dx != dy && -dx != dy { // Only allows cardinal directions
					newPoint := Point{
						x: p.x + dx,
						y: p.y + dy,
					}

					dp := DistancePoint{
						Point: newPoint,
						Distance: Distance{
							distance: p.distance + 1,
							origin:   p.origin,
						},
					}

					if distanceMap.Update(dp) {
						q.Push(dp)
					}
				}
			}
		}
	}

	return distanceMap
}

func rejectInfinite(points []Point, distances *Map) []Point {
	invalidPoints := make(map[int]bool)

	rawDistances := &distances.distances

	for x := 0; x < distances.bounds.Width(); x++ {
		invalidPoints[(*rawDistances)[x][0].origin] = true
		invalidPoints[(*rawDistances)[x][distances.bounds.Height()-1].origin] = true
	}

	for y := 0; y < distances.bounds.Height(); y++ {
		invalidPoints[(*rawDistances)[0][y].origin] = true
		invalidPoints[(*rawDistances)[distances.bounds.Width()-1][y].origin] = true
	}

	result := make([]Point, 0)
	for id, p := range points {
		if !invalidPoints[id] {
			result = append(result, p)
		}
	}

	return result
}

func distanceByPoint(distanceMap *Map, points []Point) map[Point]int {
	distances := make(map[Point]int)

	rawMap := &distanceMap.distances

	for _, col := range *rawMap {
		for _, el := range col {
			if el.state == Visited {
				distances[points[el.origin]]++
			}
		}
	}
	return distances
}

func findMaxEligible(distances map[Point]int, eligible []Point) int {
	max := -1

	for _, p := range eligible {
		value := distances[p]
		if value > max {
			max = value
		}
	}

	return max
}

func main() {
	allPoints := read()

	bounds := pointBounds(allPoints)

	distanceMap := bfs(allPoints, bounds)

	pointDistances := distanceByPoint(distanceMap, allPoints)

	validPoints := rejectInfinite(allPoints, distanceMap)

	result := findMaxEligible(pointDistances, validPoints)
	fmt.Println(result)
}
