# Advent of Code 2018 in Go


Here are my [https://adventofcode.com/2018](Advent of Code 2018) solutions written in Go.


My personal goal for this year:

- Learn a new language (Go) and stick with it,
- Try to implement efficient algorithms, even if the problem can be solved with something simpler, but not as efficient.


I have solved the first few days in Python before deciding on this challenge, so days 1 and 2 have bonus Python solutions!