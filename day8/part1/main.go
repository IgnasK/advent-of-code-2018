package main

import "fmt"

func read() []int {
	result := make([]int, 0)

	for {
		var value int
		if _, err := fmt.Scanf("%d", &value); err != nil {
			break
		}

		result = append(result, value)
	}

	return result
}

// Outputs end pos and count
func metadataCount(values []int) (int, int) {
	children := values[0]
	metadata := values[1]

	count := 0
	currentIndex := 2
	for i := 0; i < children; i++ {
		recPos, recCount := metadataCount(values[currentIndex:])
		currentIndex += recPos
		count += recCount
	}

	for i := 0; i < metadata; i++ {
		count += values[currentIndex]
		currentIndex++
	}

	return currentIndex, count
}

func main() {
	values := read()

	_, count := metadataCount(values)

	fmt.Println(count)
}
