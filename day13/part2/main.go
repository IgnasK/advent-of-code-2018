package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

type Direction int

const (
	north Direction = 0
	east  Direction = 1
	south Direction = 2
	west  Direction = 3
)

func turn(direction Direction, amount int) Direction {
	return Direction((int(direction) + amount + 4) % 4)
}

func directionRepr(direction Direction) rune {
	if direction == north {
		return '^'
	}
	if direction == east {
		return '>'
	}
	if direction == south {
		return 'v'
	}
	if direction == west {
		return '<'
	}
	return 0
}

type Cart struct {
	x, y      int
	direction Direction
	lastTurn  int
}

type Map [][]rune

func (m *Map) Print(carts []*Cart) {
	cartsMap := make(map[int]map[int]*Cart)
	for _, c := range carts {
		if cartsMap[c.y] == nil {
			cartsMap[c.y] = make(map[int]*Cart)
		}
		cartsMap[c.y][c.x] = c
	}

	for y := 0; y < len(*m); y++ {
		for x := 0; x < len((*m)[y]); x++ {
			if c := cartsMap[y][x]; c != nil {
				fmt.Print(string(directionRepr(c.direction)))
			} else {
				fmt.Print(string((*m)[y][x]))
			}
		}
		fmt.Println()
	}
}

func nextTurnType(previous int) int {
	return (previous+2)%3 - 1
}

func (c *Cart) Advance(m *Map) {
	if c.direction == north {
		c.y--
	} else if c.direction == east {
		c.x++
	} else if c.direction == south {
		c.y++
	} else if c.direction == west {
		c.x--
	}

	if (*m)[c.y][c.x] == '\\' {
		if c.direction == east || c.direction == west {
			c.direction = turn(c.direction, 1)
		} else {
			c.direction = turn(c.direction, -1)
		}
	}
	if (*m)[c.y][c.x] == '/' {
		if c.direction == east || c.direction == west {
			c.direction = turn(c.direction, -1)
		} else {
			c.direction = turn(c.direction, 1)
		}
	}

	if (*m)[c.y][c.x] == '+' {
		turnDirection := nextTurnType(c.lastTurn)
		c.direction = turn(c.direction, turnDirection)
		c.lastTurn = turnDirection
	}
}

func hasCrashed(c *Cart, others []*Cart) *Cart {
	for _, other := range others {
		if c == other {
			continue
		}
		if c.x == other.x && c.y == other.y {
			return other
		}
	}

	return nil
}

func simulateSingle(m *Map, cart *Cart) {
	cart.Advance(m)
}

// Returns a cart that crashed
func simulateAll(m *Map, carts []*Cart) map[*Cart]bool {
	sort.Slice(carts, func(i, j int) bool {
		if carts[i].y == carts[j].y {
			return carts[i].x < carts[j].x
		}
		return carts[i].y < carts[j].y
	})

	crashedCarts := make(map[*Cart]bool)

	for _, cart := range carts {
		if crashedCarts[cart] {
			continue
		}

		simulateSingle(m, cart)
		if other := hasCrashed(cart, carts); other != nil {
			crashedCarts[cart] = true
			crashedCarts[other] = true
		}
	}

	return crashedCarts
}

func readCart(r rune, x int, y int) (*Cart, rune) {
	if r == '^' {
		return &Cart{x: x, y: y, direction: north, lastTurn: 1}, '|'
	}
	if r == '>' {
		return &Cart{x: x, y: y, direction: east, lastTurn: 1}, '-'
	}
	if r == 'v' {
		return &Cart{x: x, y: y, direction: south, lastTurn: 1}, '|'
	}
	if r == '<' {
		return &Cart{x: x, y: y, direction: west, lastTurn: 1}, '-'
	}
	return nil, 0
}

func parseCarts(line []rune, y int) ([]rune, []*Cart) {
	results := make([]*Cart, 0)

	for i, v := range line {
		if c, replacement := readCart(v, i, y); replacement != 0 {
			results = append(results, c)
			line[i] = replacement
		}
	}
	return line, results
}

func read() (Map, []*Cart) {
	m := make([][]rune, 0)
	c := make([]*Cart, 0)

	scanner := bufio.NewScanner(os.Stdin)
	for i := 0; scanner.Scan(); i++ {
		v := scanner.Text()
		line, carts := parseCarts([]rune(v), i)
		m = append(m, line)
		c = append(c, carts...)
	}

	return m, c
}

func main() {
	m, carts := read()
	for len(carts) > 1 {
		//m.Print(carts)
		crashed := simulateAll(&m, carts)
		newCarts := make([]*Cart, 0)
		for _, cart := range carts {
			if !crashed[cart] {
				newCarts = append(newCarts, cart)
			}
		}
		carts = newCarts
	}

	fmt.Printf("%d,%d\n", carts[0].x, carts[0].y)
}
