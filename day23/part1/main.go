package main

import "fmt"

type Nanobot struct {
	x, y, z int
	radius  int
}

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

func (b1 Nanobot) Distance(b2 Nanobot) int {
	return abs(b1.x-b2.x) + abs(b1.y-b2.y) + abs(b1.z-b2.z)
}

func read() []Nanobot {
	result := make([]Nanobot, 0)

	for {
		var bot Nanobot
		if _, err := fmt.Scanf("pos=<%d,%d,%d>, r=%d\n", &bot.x, &bot.y, &bot.z, &bot.radius); err != nil {
			break
		}

		result = append(result, bot)
	}

	return result
}

func strongest(bots []Nanobot) Nanobot {
	result := bots[0]

	for _, b := range bots {
		if b.radius > result.radius {
			result = b
		}
	}

	return result
}

func inRange(bots []Nanobot, target Nanobot) int {
	result := 0

	for _, b := range bots {
		if b.Distance(target) <= target.radius {
			result++
		}
	}

	return result
}

func main() {
	bots := read()
	strongestBot := strongest(bots)
	result := inRange(bots, strongestBot)
	fmt.Println(result)
}
