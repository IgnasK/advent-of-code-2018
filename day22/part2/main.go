package main // import "bitbucket.org/sangi93/advent-of-code"

import (
	"container/heap"
	"fmt"
)

type Point struct {
	x, y int
}

func (p Point) Add(q Point) Point {
	return Point{
		p.x + q.x,
		p.y + q.y,
	}
}

type Map [][]int

func NewMap(target Point, value int) Map {
	w := 1000
	h := 1000
	result := make(Map, h)
	for y := 0; y < h; y++ {
		result[y] = make([]int, w)
		for x := 0; x < w; x++ {
			result[y][x] = value
		}
	}

	return result
}

var symbols = []string{".", "=", "|"}

func (m Map) Print() {
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[y]); x++ {
			fmt.Print(symbols[m[y][x]])
		}
		fmt.Println()
	}
}

func (m Map) PrintNumbers() {
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[y]); x++ {
			fmt.Print(m[y][x])
			fmt.Print("\t")
		}
		fmt.Println()
	}
}

func (m Map) Sum() int {
	result := 0
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[y]); x++ {
			result += m[y][x]
		}
	}

	return result
}

const modulo = 20183

func generateMap(depth int, target Point) Map {
	m := NewMap(target, 0)

	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[y]); x++ {
			if y == 0 {
				m[y][x] = (x * 16807) % modulo
			} else if x == 0 {
				m[y][x] = (y * 48271) % modulo
			} else if x == target.x && y == target.y {
				m[y][x] = 0
			} else {
				m[y][x] = (m[y-1][x] * m[y][x-1]) % modulo
			}
			m[y][x] = (m[y][x] + depth) % modulo
		}
	}

	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[y]); x++ {
			m[y][x] %= 3
		}
	}

	return m
}

type Tool int

const (
	Neither      Tool = iota
	ClimbingGear Tool = iota
	Torch        Tool = iota
)

type Node struct {
	Point
	tool     Tool
	distance int
}

type PQNode struct {
	index int
	Node
}

type PriorityQueue []*PQNode

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].distance < pq[j].distance
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := &PQNode{n, x.(Node)}
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item.Node
}

const INF = 100000

var directions = []Point{
	{0, -1},
	{1, 0},
	{0, 1},
	{-1, 0},
}

func canAdvance(area int, tool Tool) bool {
	if area == 0 && (tool == ClimbingGear || tool == Torch) {
		return true
	}
	if area == 1 && (tool == ClimbingGear || tool == Neither) {
		return true
	}
	if area == 2 && (tool == Neither || tool == Torch) {
		return true
	}

	return false
}

func solve(m Map, target Point) int {
	maps := make([]Map, 3)
	for t := 0; t < 3; t++ {
		maps[t] = NewMap(target, INF)
	}

	pq := make(PriorityQueue, 0)
	heap.Init(&pq)
	heap.Push(&pq, Node{Point{0, 0}, Torch, 0})
	maps[Torch][0][0] = 0

	for pq.Len() > 0 {
		node := heap.Pop(&pq).(Node)
		//fmt.Println(node)
		if maps[node.tool][node.y][node.x] < node.distance {
			continue
		}
		if node.x == target.x && node.y == target.y && node.tool == Torch {
			return node.distance
		}

		for t := 0; t < 3; t++ {
			if canAdvance(m[node.y][node.x], Tool(t)) {
				distance := node.distance + 7
				if maps[t][node.y][node.x] > distance {
					maps[t][node.y][node.x] = distance
					heap.Push(&pq, Node{node.Point, Tool(t), distance})
				}
			}

		}

		for _, dir := range directions {
			newPos := node.Point.Add(dir)
			if newPos.x >= 0 && newPos.y >= 0 {
				if !canAdvance(m[newPos.y][newPos.x], node.tool) {
					continue
				}
				distance := node.distance + 1

				if maps[node.tool][newPos.y][newPos.x] > distance {
					maps[node.tool][newPos.y][newPos.x] = distance
					heap.Push(&pq, Node{newPos, node.tool, distance})
				}
			}
		}
	}

	return -1
}

func read() (int, Point) {
	var depth int
	var target Point

	fmt.Scanf("depth: %d", &depth)
	fmt.Scanf("target: %d,%d", &target.x, &target.y)

	return depth, target
}

func main() {
	depth, target := read()
	m := generateMap(depth, target)
	fmt.Println(solve(m, target))
}
