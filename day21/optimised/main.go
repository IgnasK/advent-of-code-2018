package main

import "fmt"

func convert(b bool) int {
	if b {
		return 1
	}
	return 0
}

func solve() (int, int) {
	seen := make(map[int]bool)
	last := -1
	first := -1

	a := 0
	a = a
	b := 0
	b = b
	c := 0
	c = c
	d := 0
	d = d
	e := 0
	e = e
	f := 0
	d = 123
p1:
	d = d & 456
	d = convert(d == 72)
	if d == 1 {
		goto p5
	}
	goto p1
p5:
	d = 0
p6:
	c = d | 65536
	d = 1397714
p8:
	f = c & 255
	d = d + f
	d = d & 16777215
	d = d * 65899
	d = d & 16777215
	f = convert(256 > c)
	if f == 1 {
		goto p16
	}
	goto p17
p16:
	goto p28
p17:
	f = 0
p18:
	b = f + 1
	b = b * 256
	b = convert(b > c)
	if b == 1 {
		goto p23
	}
	goto p24
p23:
	goto p26
p24:
	f = f + 1
	goto p18
p26:
	c = f
	goto p8
p28:
	f = convert(d == a)

	if seen[d] {
		return first, last
	}
	seen[d] = true
	last = d
	if first == -1 {
		first = d
	}

	if f == 1 {
		goto p31
	}
	goto p6
p31:

	return first, last
}

func main() {
	fmt.Println(solve())
}
