# Implementation

As well as solving the problems using the interpreter developed for day 19,
I have also created a transpiler for ElfCode (found in generator directory),
that prints out go code corresponding to the ElfCode input.

# Compute times

Part 1:

* Interpreted: 7ms

* Transpiled: 5ms

Part 2:

* Interpreted: 1m 32s

* Transpiled: 250ms
