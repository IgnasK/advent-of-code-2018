package main

import (
	"fmt"
	"strings"
)

func split(a, b int) []int {
	sum := a + b
	if sum >= 10 {
		return []int{sum / 10, sum % 10}
	} else {
		return []int{sum}
	}
}

func combine(s []int) string {
	combined := strings.Builder{}
	for _, v := range s {
		combined.WriteRune(rune('0' + v))
	}

	return combined.String()
}

func solve(needle string) int {
	arr := []int{3, 7}

	a := 0
	b := 1

	for {
		recipeA := arr[a]
		recipeB := arr[b]
		recipes := split(recipeA, recipeB)
		arr = append(arr, recipes...) // Either returns 1 or 2 elements

		// Check either last len(needle) elements or len(needle)+1 elements, since the answer could be finished by
		// The first element of the split
		if index := len(arr) - len(needle) - 1; len(arr) > len(needle) && combine(arr[index:len(arr)-1]) == needle {
			return index
		} else if index := len(arr) - len(needle); len(arr) >= len(needle) && combine(arr[index:len(arr)]) == needle {
			return index
		}

		a = (a + 1 + recipeA) % len(arr)
		b = (b + 1 + recipeB) % len(arr)
	}

	return 0
}

func read() string {
	var result string
	fmt.Scanf("%s", &result)
	return result
}

func main() {
	cnt := read()
	fmt.Println(solve(cnt))
}
