package main

import (
	"fmt"
	"strings"
)

func split(a, b int) []int {
	sum := a + b
	if sum >= 10 {
		return []int{sum / 10, sum % 10}
	} else {
		return []int{sum}
	}
}

func combine(s []int) string {
	combined := strings.Builder{}
	for _, v := range s {
		combined.WriteRune(rune('0' + v))
	}

	return combined.String()
}

func solve(count int) string {
	arr := []int{3, 7}

	a := 0
	b := 1

	for len(arr) < count+10 {
		recipeA := arr[a]
		recipeB := arr[b]
		recipes := split(recipeA, recipeB)
		arr = append(arr, recipes...)

		a = (a + 1 + recipeA) % len(arr)
		b = (b + 1 + recipeB) % len(arr)
	}

	return combine(arr[count : count+10])
}

func read() int {
	var result int
	fmt.Scanf("%d", &result)
	return result
}

func main() {
	cnt := read()
	fmt.Println(solve(cnt))
}
