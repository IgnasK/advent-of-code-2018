package main

import (
	"fenwick"
	"fmt"
)

type rect struct {
	x, y, w, h int
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func (this *rect) extend(that rect) {
	right := this.x + this.w
	bottom := this.y + this.h
	if that.x < this.x {
		this.x = that.x
	}
	if that.y < this.y {
		this.y = that.y
	}
	newRight := max(right, that.x+that.w)
	newBottom := max(bottom, that.y+that.h)
	this.w = newRight - this.x
	this.h = newBottom - this.y
}

type claimrect struct {
	claim int
	rect
}

func readRects() []rect {
	rects := make([]rect, 0)

	for {
		var r rect
		var claim int
		if _, err := fmt.Scanf("#%d @ %d,%d: %dx%d", &claim, &r.x, &r.y, &r.w, &r.h); err != nil {
			break
		}
		rects = append(rects, r)
	}
	return rects
}

func boundingBox(rects []rect) rect {
	bounds := rects[0]
	for _, el := range rects {
		bounds.extend(el)
	}
	return bounds
}

func updateFenwick(fenwick *fenwick.Fenwick2D, bounds rect, rect rect) {
	x := rect.x - bounds.x
	y := rect.y - bounds.y
	right := x + rect.w
	bottom := y + rect.h
	fenwick.UpdateRange(x, y, right, bottom, 1)
}

func main() {
	rects := readRects()
	bb := boundingBox(rects)

	// I know this can be solved with a 2D array and postprocessing
	// I'm using this problem as an opportunity to implement my favourite data structure in go:
	// Fenwick (Binary Index) Trees.
	fenwick := fenwick.NewFenwick2D(bb.w, bb.h)
	for _, r := range rects {
		updateFenwick(fenwick, bb, r)
	}

	sum := 0
	for y := 0; y < bb.h; y++ {
		for x := 0; x < bb.w; x++ {
			hits := fenwick.Get(x, y)
			if hits >= 2 {
				sum++
			}
		}
	}

	fmt.Print(sum)
}
