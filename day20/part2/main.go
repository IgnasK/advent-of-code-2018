package main

import (
	"encoding/json"
	"fmt"
)

type Direction rune

// RegexChar is either a single value or a list of options
type RegexChar struct {
	Value   Direction `json:"value"`
	Options []Regex   `json:"options"`
}

// Regex is a list of RegexChars
type Regex []RegexChar

func parse(values []Direction) []Regex {
	result := make([]Regex, 0)

	currentRegex := make([]RegexChar, 0)

	i := 0

	for ; i < len(values); i++ {
		v := values[i]

		if v == '(' {
			numOpen := 1
			j := i + 1
			for ; j < len(values); j++ {

				v2 := values[j]
				if v2 == '(' {
					numOpen++
				}
				if v2 == ')' {
					numOpen--
				}
				if numOpen == 0 {
					break
				}
			}
			options := parse(values[i+1 : j])
			currentRegex = append(currentRegex, RegexChar{'\000', options})
			i = j
			continue
		}

		if v == '|' {
			result = append(result, Regex(currentRegex))
			currentRegex = make([]RegexChar, 0)
			continue
		}

		currentRegex = append(currentRegex, RegexChar{v, []Regex{}})
	}

	result = append(result, Regex(currentRegex))

	return result
}

func read() Regex {

	var line string
	fmt.Scanf("^%s$", &line)

	options := parse([]Direction(line))
	if len(options) == 1 {
		return options[0]
	}

	return Regex([]RegexChar{RegexChar{'\000', options}})
}

func prettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}

// Room represents a single room
type Room struct {
	x, y int
}

func (r Room) Traverse(d Direction) Room {
	if d == 'N' {
		r.y--
	} else if d == 'E' {
		r.x++
	} else if d == 'S' {
		r.y++
	} else if d == 'W' {
		r.x--
	}
	return r
}

// Door is a pair of rooms that can be directly moved to from the other
type Door struct {
	a, b Room
}

// State contains all seen rooms and doors
type State struct {
	rooms map[Room]bool
	doors map[Door]bool
}

type Bounds struct {
	left, right, top, bottom int
}

func (b Bounds) Width() int {
	return b.right - b.left + 1
}

func (b Bounds) Height() int {
	return b.bottom - b.top + 1
}

const INF = 100000000

func NewBounds() Bounds {
	return Bounds{INF, -INF, INF, -INF}
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func (s *State) Bounds() Bounds {
	b := NewBounds()
	for r := range s.rooms {
		b.left = min(b.left, r.x)
		b.right = max(b.right, r.x)
		b.top = min(b.top, r.y)
		b.bottom = max(b.bottom, r.y)
	}

	return b
}

func (s *State) Print() {
	bounds := s.Bounds()
	fmt.Println(bounds)

	for x := 0; x <= 2*bounds.Width(); x++ {
		fmt.Print("#")
	}
	fmt.Println()

	for y := 0; y < bounds.Height(); y++ {
		fmt.Print("#")
		for x := 0; x < bounds.Width(); x++ {
			v := Room{x + bounds.left, y + bounds.top}
			vNext := v.Traverse('E')
			if s.rooms[v] {
				fmt.Print(".")
			} else {
				fmt.Print("#")
			}

			if s.doors[Door{v, vNext}] {
				fmt.Print("|")
			} else {
				fmt.Print("#")
			}
		}
		fmt.Println()
		fmt.Print("#")
		for x := 0; x < bounds.Width(); x++ {
			v := Room{x + bounds.left, y + bounds.top}
			vNext := v.Traverse('S')
			if s.doors[Door{v, vNext}] {
				fmt.Print("-")
			} else {
				fmt.Print("#")
			}

			fmt.Print("#")
		}
		fmt.Println()
	}
}

type Edges map[Room]bool

// Merge merges values of two states into the first one
func (s *Edges) Merge(other Edges) {
	for r := range other {
		(*s)[r] = true
	}
}

func (s *Edges) Copy() Edges {
	result := make(Edges)
	for v := range *s {
		result[v] = true
	}

	return result
}

func evaluateSingle(rc RegexChar, s *State, edge Edges) Edges {
	result := make(Edges)

	if rc.Value != '\000' {
		for v := range edge {
			newV := v.Traverse(rc.Value)

			result[newV] = true
			s.rooms[newV] = true
			s.doors[Door{v, newV}] = true
			s.doors[Door{newV, v}] = true
		}
	} else {
		for _, r := range rc.Options {
			state := edge.Copy()
			result.Merge(evaluate(r, s, state))
		}
	}

	return result
}

func evaluate(r Regex, s *State, edge Edges) Edges {
	for _, rc := range r {
		edge = evaluateSingle(rc, s, edge)
	}
	return edge
}

func solve(r Regex) int {
	edge := Edges{Room{0, 0}: true}
	state := State{Edges{Room{0, 0}: true}, make(map[Door]bool)}

	evaluate(r, &state, edge)

	return furthestDistance(&state)
}

type Queue []interface{}

func (q *Queue) Push(d interface{}) {
	*q = append(*q, d)
}

func (q *Queue) Size() int {
	return len(*q)
}

func (q *Queue) Empty() bool {
	return q.Size() == 0
}

func (q *Queue) Back() interface{} {
	return []interface{}(*q)[0]
}

func (q *Queue) Pop() interface{} {
	back := q.Back()
	*q = []interface{}(*q)[1:]
	return back
}

var directions = []Direction("NESW")

func furthestDistance(s *State) int {
	visited := make(map[Room]bool)
	distance := make(map[Room]int)

	room := Room{0, 0}
	visited[room] = true
	distance[room] = 0

	q := make(Queue, 0)
	q.Push(room)

	result := 0

	for !q.Empty() {
		room := q.Pop().(Room)

		if distance[room] >= 1000 {
			result++
		}

		for _, d := range directions {
			newRoom := room.Traverse(d)
			if s.doors[Door{room, newRoom}] && !visited[newRoom] {
				visited[newRoom] = true
				distance[newRoom] = distance[room] + 1
				q.Push(newRoom)
			}
		}
	}

	return result
}

func main() {
	parsed := read()
	result := solve(parsed)
	fmt.Println(result)
}
