import sys
from collections import defaultdict

def counts(word):
    cts = defaultdict(int)
    for c in word:
        cts[c] += 1

    return cts

def has_value(cts, value):
    for _, v in cts.items():
        if v == value:
            return True
    return False

twos = 0
threes = 0
for line in sys.stdin:
    cts = counts(line.strip())
    if has_value(cts, 2):
        twos += 1
    if has_value(cts, 3):
        threes += 1

print(twos * threes)