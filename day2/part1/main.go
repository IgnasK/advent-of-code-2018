package main

import "fmt"

func read() []string {
	results := make([]string, 0)
	for {
		var res string
		if _, err := fmt.Scanf("%s", &res); err != nil {
			break
		}

		results = append(results, res)
	}

	return results
}

func counts(str string) map[rune]int {
	result := make(map[rune]int)
	for _, rune := range str {
		result[rune]++
	}
	return result
}

func hasValue(m map[rune]int, value int) bool {
	for _, count := range m {
		if count == value {
			return true
		}
	}

	return false
}

func main() {
	values := read()

	var numTwos, numThrees int
	for _, str := range values {
		cnt := counts(str)

		if hasValue(cnt, 2) {
			numTwos++
		}
		if hasValue(cnt, 3) {
			numThrees++
		}
	}

	fmt.Println(numTwos * numThrees)
}
