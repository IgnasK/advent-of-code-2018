package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

type Bounds struct {
	left, right, top, bottom int
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func abs(a int) int {
	if a > 0 {
		return a
	} else {
		return -a
	}
}

func (bounds Bounds) Extend(other Bounds) Bounds {
	bounds.left = min(bounds.left, other.left)
	bounds.right = max(bounds.right, other.right)
	bounds.top = min(bounds.top, other.top)
	bounds.bottom = max(bounds.bottom, other.bottom)
	return bounds
}

func (bounds *Bounds) Width() int {
	return bounds.right - bounds.left + 1
}

func (bounds *Bounds) Height() int {
	return bounds.bottom - bounds.top + 1
}

type Map struct {
	values [][]rune
	bounds Bounds
}

func NewMap(bounds Bounds) *Map {
	values := make([][]rune, bounds.Height()+2)
	for i := range values {
		values[i] = make([]rune, bounds.Width()+2)
		for j := range values[i] {
			values[i][j] = '.'
		}
	}
	return &Map{values: values, bounds: bounds}
}

func (m *Map) Get(x, y int) rune {
	normX := x - m.bounds.left + 1
	normY := y - m.bounds.top + 1
	if normX < 0 || normX > m.bounds.Width()+1 ||
		normY < 0 || normY > m.bounds.Height()+1 {
		return '.'
	}

	return m.values[normY][normX]
}

func (m *Map) Set(x, y int, r rune) {
	normX := x - m.bounds.left + 1
	normY := y - m.bounds.top + 1
	if normX < 0 || normX > m.bounds.Width()+1 ||
		normY < 0 || normY > m.bounds.Height()+1 {
		return
	}

	m.values[normY][normX] = r
}

func (m *Map) Print() {
	for y := m.bounds.top - 1; y <= m.bounds.bottom+1; y++ {
		for x := m.bounds.left - 1; x <= m.bounds.right+1; x++ {
			fmt.Print(string(m.Get(x, y)))
		}
		fmt.Println()
	}
}

func (m *Map) PrintAround(px, py int) {
	w := bufio.NewWriter(os.Stdout)
	defer w.Flush()
	for y := py - 15; y <= py+15; y++ {
		for x := px - 20; x <= px+20; x++ {
			w.WriteRune(m.Get(x, y))
		}
		w.WriteRune('\n')
	}
}

func (m *Map) NumReachable() int {
	result := 0
	for y := m.bounds.top; y <= m.bounds.bottom; y++ {
		for x := m.bounds.left - 1; x <= m.bounds.right+1; x++ {
			v := m.Get(x, y)
			if v == '~' {
				result++
			}
		}
	}
	return result
}

func read() []Bounds {
	result := make([]Bounds, 0)

	for {
		var t, u rune
		var a, b, c int
		if _, err := fmt.Scanf("%c=%d, %c=%d..%d\n", &t, &a, &u, &b, &c); err != nil {
			break
		}
		var bounds Bounds
		if t == 'x' {
			bounds = Bounds{left: a, right: a, top: b, bottom: c}
		} else {
			bounds = Bounds{left: b, right: c, top: a, bottom: a}
		}

		result = append(result, bounds)
	}

	return result
}

func boundingBox(bounds []Bounds) Bounds {
	result := bounds[0]
	for _, v := range bounds[1:] {
		result = result.Extend(v)
	}
	return result
}

func fill(m *Map, bounds Bounds, value rune) {
	for y := bounds.top; y <= bounds.bottom; y++ {
		for x := bounds.left; x <= bounds.right; x++ {
			m.Set(x, y, value)
		}
	}
}

func buildMap(values []Bounds, bounds Bounds) *Map {
	m := NewMap(bounds)
	for _, v := range values {
		fill(m, v, '#')
	}

	return m
}

type Point struct {
	x, y int
}

type Stack []interface{}

func (q *Stack) Push(d interface{}) {
	*q = append(*q, d)
}

func (q *Stack) Size() int {
	return len(*q)
}

func (q *Stack) Empty() bool {
	return q.Size() == 0
}

func (q *Stack) Top() interface{} {
	return []interface{}(*q)[q.Size()-1]
}

func (q *Stack) Pop() interface{} {
	front := q.Top()
	*q = []interface{}(*q)[:q.Size()-1]
	return front
}

const INF int = 1000000

func findEdges(m *Map, point Point) (int, int) {
	left := -INF
	right := INF
	for x := point.x; x <= m.bounds.right+1; x++ {
		below := m.Get(x, point.y+1)
		if below == '|' || below == '.' {
			break
		}
		if m.Get(x, point.y) == '#' {
			right = x - 1
			break
		}
	}
	for x := point.x; x >= m.bounds.left-1; x-- {
		below := m.Get(x, point.y+1)
		if below == '|' || below == '.' {
			break
		}
		if m.Get(x, point.y) == '#' {
			left = x + 1
			break
		}
	}

	return left, right
}

func findFalls(m *Map, point Point) (int, int) {
	left := -INF
	right := INF
	for x := point.x; x <= m.bounds.right+1; x++ {
		below := m.Get(x, point.y+1)
		if m.Get(x, point.y) == '#' || below == '|' {
			break
		}
		if below == '.' {
			right = x
			break
		}
	}
	for x := point.x; x >= m.bounds.left-1; x-- {
		below := m.Get(x, point.y+1)
		if m.Get(x, point.y) == '#' || below == '|' {
			break
		}
		if below == '.' {
			left = x
			break
		}
	}

	return left, right
}

func simulate(m *Map, spring Point) {

	spring.y = max(spring.y, m.bounds.top)

	q := make(Stack, 0)
	q.Push(spring)

	for i := 0; !q.Empty(); i++ {
		p := q.Top().(Point)

		if animate && i > animateStart && i%animateFreq == 0 {
			fmt.Print("\033[H\033[2J")
			fmt.Println(p)
			if animateClose {
				m.PrintAround(p.x, p.y)
			} else {
				m.Print()
			}
			time.Sleep(100 * time.Millisecond)
		}

		value := m.Get(p.x, p.y)
		if value == '#' {
			fmt.Println("Wall at", p)
			break
		}
		if p.y < m.bounds.top || p.y > m.bounds.bottom {
			m.Set(p.x, p.y, '|')
			q.Pop()
			continue
		}

		below := m.Get(p.x, p.y+1)
		if below == '.' { // Empty space, fall below
			m.Set(p.x, p.y, '|')
			q.Push(Point{x: p.x, y: p.y + 1})
		} else if below == '|' {
			m.Set(p.x, p.y, '|')
			q.Pop()
		} else if below == '#' || below == '~' { // Ground below, spread
			// Try to fill the whole layer
			leftEdge, rightEdge := findEdges(m, p)
			if leftEdge != -INF && rightEdge != INF {
				fill(m, Bounds{left: leftEdge, right: rightEdge, top: p.y, bottom: p.y}, '~')
				q.Pop()
			} else {
				//Can't fill the whole layer, find a drop
				leftDrop, rightDrop := findFalls(m, p)
				if leftDrop == -INF && rightDrop == INF {
					// There is no drop, abort the layer
					q.Pop()
				}
				if leftDrop > -INF {
					q.Push(Point{x: leftDrop, y: p.y + 1})
				}
				if rightDrop < INF {
					q.Push(Point{x: rightDrop, y: p.y + 1})
				}
				fillBounds := Bounds{
					left:   max(leftEdge, leftDrop),
					right:  min(rightEdge, rightDrop),
					top:    p.y,
					bottom: p.y,
				}
				if fillBounds.left != -INF && fillBounds.right != INF {
					fill(m, fillBounds, '|')
				}
			}
		}
	}
}

const animate bool = false
const animateFreq = 1
const animateStart = 0
const animateClose = false

func main() {
	areas := read()
	bounds := boundingBox(areas)
	m := buildMap(areas, bounds)
	simulate(m, Point{500, 0})
	fmt.Println(m.NumReachable())
}
