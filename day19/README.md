# This day has 2 solutions for part 1:

* Interpretation (in directory part1): executes in 250ms

* Transpiled (in directory optimised): executes in 7ms

The transpiled solution was generated using a transpiler from day 21.

# Part 2

For part 2 I have reverse engineered the code to find that it (rather inefficiently)
computes the sum of all factors of a large numer. I have instead simulated the program
for several iterations to find the large value, and then efficiently compute the result.

I did not compute the runtime of interpreted solution for this part. Transpiled solution runs for 1d 22h.
