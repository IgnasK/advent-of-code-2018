package main

import "fmt"

func convert(b bool) int {
	if b {
		return 1
	}
	return 0
}

func solve() int {
	a := 0
	a = a
	b := 0
	b = b
	c := 0
	c = c
	d := 0
	d = d
	e := 0
	e = e
	f := 0
	f = f
	goto p17
p1:
	d = 1
p2:
	b = 1
p3:
	c = d * b
p4:
	c = convert(c == f)
p5:
	if c == 1 {
		goto p7
	}
p6:
	goto p8
p7:
	a = d + a
p8:
	b = b + 1
p9:
	c = convert(b > f)
p10:
	if c == 1 {
		goto p12
	}
p11:
	goto p3
p12:
	d = d + 1
p13:
	c = convert(d > f)
p14:
	if c == 1 {
		goto p16
	}
p15:
	goto p2
p16:
	e = 16
	e = e * e
	if e == 0 {
		goto p1
	}
	if e == 1 {
		goto p2
	}
	if e == 2 {
		goto p3
	}
	if e == 3 {
		goto p4
	}
	if e == 4 {
		goto p5
	}
	if e == 5 {
		goto p6
	}
	if e == 6 {
		goto p7
	}
	if e == 7 {
		goto p8
	}
	if e == 8 {
		goto p9
	}
	if e == 9 {
		goto p10
	}
	if e == 10 {
		goto p11
	}
	if e == 11 {
		goto p12
	}
	if e == 12 {
		goto p13
	}
	if e == 13 {
		goto p14
	}
	if e == 14 {
		goto p15
	}
	if e == 15 {
		goto p16
	}
	if e == 16 {
		goto p17
	}
	if e == 17 {
		goto p18
	}
	if e == 18 {
		goto p19
	}
	if e == 19 {
		goto p20
	}
	if e == 20 {
		goto p21
	}
	if e == 21 {
		goto p22
	}
	if e == 22 {
		goto p23
	}
	if e == 23 {
		goto p24
	}
	if e == 24 {
		goto p25
	}
	if e == 25 {
		goto p26
	}
	if e == 26 {
		goto p27
	}
	if e == 27 {
		goto p28
	}
	if e == 28 {
		goto p29
	}
	if e == 29 {
		goto p30
	}
	if e == 30 {
		goto p31
	}
	if e == 31 {
		goto p32
	}
	if e == 32 {
		goto p33
	}
	if e == 33 {
		goto p34
	}
	if e == 34 {
		goto p35
	}
	if e == 35 {
		goto p36
	}
	if e >= 36 {
		goto p36
	}
p17:
	f = f + 2
p18:
	f = f * f
p19:
	e = 19
	f = e * f
p20:
	f = f * 11
p21:
	e = 21
	c = c + 4
p22:
	e = 22
	c = c * e
p23:
	c = c + 5
p24:
	f = f + c
p25:
	e = 25
	e = e + a
	if e == 0 {
		goto p1
	}
	if e == 1 {
		goto p2
	}
	if e == 2 {
		goto p3
	}
	if e == 3 {
		goto p4
	}
	if e == 4 {
		goto p5
	}
	if e == 5 {
		goto p6
	}
	if e == 6 {
		goto p7
	}
	if e == 7 {
		goto p8
	}
	if e == 8 {
		goto p9
	}
	if e == 9 {
		goto p10
	}
	if e == 10 {
		goto p11
	}
	if e == 11 {
		goto p12
	}
	if e == 12 {
		goto p13
	}
	if e == 13 {
		goto p14
	}
	if e == 14 {
		goto p15
	}
	if e == 15 {
		goto p16
	}
	if e == 16 {
		goto p17
	}
	if e == 17 {
		goto p18
	}
	if e == 18 {
		goto p19
	}
	if e == 19 {
		goto p20
	}
	if e == 20 {
		goto p21
	}
	if e == 21 {
		goto p22
	}
	if e == 22 {
		goto p23
	}
	if e == 23 {
		goto p24
	}
	if e == 24 {
		goto p25
	}
	if e == 25 {
		goto p26
	}
	if e == 26 {
		goto p27
	}
	if e == 27 {
		goto p28
	}
	if e == 28 {
		goto p29
	}
	if e == 29 {
		goto p30
	}
	if e == 30 {
		goto p31
	}
	if e == 31 {
		goto p32
	}
	if e == 32 {
		goto p33
	}
	if e == 33 {
		goto p34
	}
	if e == 34 {
		goto p35
	}
	if e == 35 {
		goto p36
	}
	if e >= 36 {
		goto p36
	}
p26:
	goto p1
p27:
	e = 27
	c = e
p28:
	e = 28
	c = c * e
p29:
	e = 29
	c = e + c
p30:
	e = 30
	c = e * c
p31:
	c = c * 14
p32:
	e = 32
	c = c * e
p33:
	f = f + c
p34:
	a = 0
p35:
	goto p1
p36:

	return a
}

func main() {
	fmt.Println(solve())
}
